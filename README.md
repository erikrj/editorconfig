# Editor Config

To download this editor config into your project, execute the following

```bash
curl -sLO https://bitbucket.org/erikrj/editorconfig/raw/master/.editorconfig
```

Please see https://editorconfig.org/ for documentation.
